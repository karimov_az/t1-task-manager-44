package ru.t1.karimov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.response.AbstractResponse;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    public TaskGetByProjectIdResponse(@NotNull final List<TaskDto> taskList) {
        this.taskList = taskList;
    }

}
