package ru.t1.karimov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.response.AbstractTaskResponse;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable final TaskDto task) {
        super(task);
    }

}
