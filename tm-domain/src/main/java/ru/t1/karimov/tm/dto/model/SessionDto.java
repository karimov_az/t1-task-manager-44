package ru.t1.karimov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class SessionDto extends AbstractUserOwnedDtoModel {

    @NotNull
    @Column(nullable = false)
    private Date date = new Date();

    @Nullable
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public SessionDto(@NotNull final String userId, @Nullable final Role role) {
        super(userId);
        this.role = role;
    }

}
