package ru.t1.karimov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.response.AbstractTaskResponse;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(@Nullable final TaskDto task) {
        super(task);
    }

}
