package ru.t1.karimov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserRemoveRequest(@Nullable final String token) {
        super(token);
    }

}
