package ru.t1.karimov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.model.ICommand;
import ru.t1.karimov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Show list arguments and commands.";

    @NotNull
    public static final String NAME = "help";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for(final ICommand command: commands) System.out.println(command);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
