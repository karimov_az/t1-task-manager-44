package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.request.task.TaskGetByIdRequest;
import ru.t1.karimov.tm.dto.response.task.TaskGetByIdResponse;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show task by id.";

    @NotNull
    public static final String NAME = "task-show-by-id";

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable final TaskDto task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
