package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.request.task.TaskGetByProjectIdRequest;
import ru.t1.karimov.tm.dto.response.task.TaskGetByProjectIdResponse;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show task by project id.";

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @NotNull final TaskGetByProjectIdResponse response = getTaskEndpoint().getTaskByProjectId(request);
        @NotNull final List<TaskDto> tasks = response.getTaskList();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
