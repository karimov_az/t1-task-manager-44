package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.component.ISaltProvider;
import ru.t1.karimov.tm.api.property.IDatabaseProperty;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseCommentsSql();

    @NotNull
    String getDataDemoLoad();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getSessionKey();

}
