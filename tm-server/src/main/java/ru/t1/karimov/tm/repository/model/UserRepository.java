package ru.t1.karimov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.model.IUserRepository;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String EMAIL = "email";

    @NotNull
    private static final String LOGIN = "login";


    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) throws Exception {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception {
        @NotNull final User user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) throws Exception {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT count(u) FROM User u";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final String jpql = "SELECT u FROM User u";
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        return query.getResultList();
    }

    @Override
    public @NotNull List<User> findAll(@NotNull final Comparator<User> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT u FROM User u ORDER BY u." + sort;
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(User.class, id);
    }

    @Override
    public @Nullable User findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT u FROM Task u";
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT u FROM User u WHERE u.login = :login";
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class)
                .setParameter(LOGIN, login)
                .setMaxResults(1);
        return query.getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT u FROM User u WHERE u.email = :email";
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class)
                .setParameter(EMAIL, email)
                .setMaxResults(1);
        return query.getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

}
