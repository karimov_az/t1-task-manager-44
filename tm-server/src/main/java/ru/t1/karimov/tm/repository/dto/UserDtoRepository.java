package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @NotNull
    private static final String EMAIL = "email";

    @NotNull
    private static final String LOGIN = "login";

    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public UserDto create(@NotNull final String login, @NotNull final String password) throws Exception {
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception {
        @NotNull final UserDto user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) throws Exception {
        @NotNull final UserDto user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT count(u) FROM UserDto u";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<UserDto> findAll() throws Exception {
        @NotNull final String jpql = "SELECT u FROM UserDto u";
        @NotNull final TypedQuery<UserDto> query = entityManager.createQuery(jpql, UserDto.class);
        return query.getResultList();
    }

    @Override
    public @NotNull List<UserDto> findAll(@NotNull final Comparator<UserDto> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT u FROM UserDto u ORDER BY u." + sort;
        @NotNull final TypedQuery<UserDto> query = entityManager.createQuery(jpql, UserDto.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public UserDto findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(UserDto.class, id);
    }

    @Override
    public @Nullable UserDto findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT u FROM UserDto u";
        @NotNull final TypedQuery<UserDto> query = entityManager.createQuery(jpql, UserDto.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @Nullable
    @Override
    public UserDto findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String jpql = "SELECT u FROM UserDto u WHERE u.login = :login";
        @NotNull final TypedQuery<UserDto> query = entityManager.createQuery(jpql, UserDto.class)
                .setParameter(LOGIN, login)
                .setMaxResults(1);
        return query.getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull final String email) throws Exception {
        @NotNull final String jpql = "SELECT u FROM UserDto u WHERE u.email = :email";
        @NotNull final TypedQuery<UserDto> query = entityManager.createQuery(jpql, UserDto.class)
                .setParameter(EMAIL, email)
                .setMaxResults(1);
        return query.getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sessionJpql = "DELETE FROM SessionDto s WHERE s.userId IN (SELECT u.id FROM UserDto u)";
        entityManager.createQuery(sessionJpql)
                .executeUpdate();
        @NotNull final String taskJpql = "DELETE FROM TaskDto t WHERE t.userId IN (SELECT u.id FROM UserDto u)";
        entityManager.createQuery(taskJpql)
                .executeUpdate();
        @NotNull final String projectJpql = "DELETE FROM ProjectDto p WHERE p.userId IN (SELECT u.id FROM UserDto u)";
        entityManager.createQuery(projectJpql)
                .executeUpdate();
        @NotNull String jpql = "DELETE FROM UserDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Override
    public void removeOne(@NotNull final UserDto user) throws Exception {
        @NotNull final String id = user.getId();
        @NotNull final String sessionJpql = "DELETE FROM SessionDto s WHERE s.userId "
                + "IN (SELECT u.id FROM UserDto u WHERE u.id = :id)";
        entityManager.createQuery(sessionJpql)
                .setParameter(ID, id)
                .executeUpdate();
        @NotNull final String taskJpql = "DELETE FROM TaskDto t WHERE t.userId "
                + "IN (SELECT u.id FROM UserDto u WHERE u.id = :id)";
        entityManager.createQuery(taskJpql)
                .setParameter(ID, id)
                .executeUpdate();
        @NotNull final String projectJpql = "DELETE FROM ProjectDto p WHERE p.userId "
                + "IN (SELECT u.id FROM UserDto u WHERE u.id = :id)";
        entityManager.createQuery(projectJpql)
                .setParameter(ID, id)
                .executeUpdate();
        entityManager.remove(user);
    }

}
