package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.repository.dto.UserDtoRepository;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final List<UserDto> userList = new ArrayList<>();

    @NotNull
    private static IUserDtoRepository userRepository;

    @Nullable
    private static EntityManager entityManager;

    @Before
    public void initRepository() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final UserDto user = new UserDto();
            user.setLogin("user" + i);
            user.setPasswordHash("user" + i);
            user.setEmail("user" + i + "@tst.ru");
            if (i <= 7) user.setRole(Role.USUAL);
            else user.setRole(Role.ADMIN);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
            userList.add(user);
        }

        entityManager.close();
    }

    @After
    public void initClear() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (@NotNull final UserDto user : userList) {
            @NotNull final String userId = user.getId();
            entityManager.getTransaction().begin();
            userRepository.removeOneById(userId);
            entityManager.getTransaction().commit();
        }
        userList.clear();

        entityManager.close();
    }

    @Test
    public void testAdd() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        final int expectedSize = userRepository.getSize().intValue();
        @NotNull final UserDto user = new UserDto();
        user.setLogin("test100");
        user.setPasswordHash("test100");
        user.setEmail("test100@tst.ru");
        entityManager.getTransaction().begin();
        assertNotNull(userRepository.add(user));
        entityManager.getTransaction().commit();
        assertEquals(expectedSize + 1, userRepository.getSize().intValue());

        @NotNull final String userId = user.getId();
        entityManager.getTransaction().begin();
        userRepository.removeOneById(userId);
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @Test
    public void testFindAll() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        final int expectedSize = userRepository.getSize().intValue();
        @NotNull final List<UserDto> actualUserList = userRepository.findAll();
        assertEquals(expectedSize, actualUserList.size());

        entityManager.close();
    }

    @Test
    public void testFindByIdPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (@NotNull final UserDto user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final UserDto actualUser = userRepository.findOneById(id);
            assertNotNull(actualUser);
            assertEquals(user.getId(), actualUser.getId());
        }

        entityManager.close();
    }

    @Test
    public void testFindByIdNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @NotNull final String id = UUID.randomUUID().toString();
        assertNull(userRepository.findOneById(id));

        entityManager.close();
    }

    @Test
    public void testFindByLoginPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (@NotNull final UserDto user : userList) {
            @Nullable final String login = user.getLogin();
            if (login == null) continue;
            @Nullable final UserDto actualUser = userRepository.findByLogin(login);
            assertNotNull(actualUser);
            assertEquals(user.getId(), actualUser.getId());
            assertEquals(login, actualUser.getLogin());
        }

        entityManager.close();
    }

    @Test
    public void testFindByLoginNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @NotNull final String login = "OtherLogin";
        assertNull(userRepository.findByLogin(login));

        entityManager.close();
    }

    @Test
    public void testFindByEmailPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (@NotNull final UserDto user : userList) {
            @Nullable final String email = user.getEmail();
            if (email == null) continue;
            @Nullable final UserDto actualUser = userRepository.findByEmail(email);
            assertNotNull(actualUser);
            assertEquals(user.getId(), actualUser.getId());
            assertEquals(email, actualUser.getEmail());
        }

        entityManager.close();
    }

    @Test
    public void testFindByEmailNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @NotNull final String email = "OtherEmail";
        assertNull(userRepository.findByEmail(email));

        entityManager.close();
    }

    @Test
    public void testIsLoginExistPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (@NotNull final UserDto user : userList) {
            @Nullable final String login = user.getLogin();
            if (login == null) continue;
            assertTrue(userRepository.isLoginExist(login));
        }

        entityManager.close();
    }

    @Test
    public void testIsLoginExistNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @Nullable final String login = "OtherLogin";
        assertFalse(userRepository.isLoginExist(login));

        entityManager.close();
    }

    @Test
    public void testisEmailExistPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        for (@NotNull final UserDto user : userList) {
            @Nullable final String email = user.getEmail();
            if (email == null) continue;
            assertTrue(userRepository.isEmailExist(email));
        }

        entityManager.close();
    }

    @Test
    public void testEmailExistNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @Nullable final String email = "OtherEmail";
        assertFalse(userRepository.isEmailExist(email));

        entityManager.close();
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        final int startedSize = userRepository.getSize().intValue();
        for (@NotNull final UserDto user : userList) {
            @Nullable final String userId = user.getId();
            entityManager.getTransaction().begin();
            userRepository.removeOneById(userId);
            entityManager.getTransaction().commit();
            assertNull(userRepository.findOneById(userId));
        }
        final int actualSize = userRepository.getSize().intValue();
        final int expectedSize = startedSize - userList.size();
        assertEquals(expectedSize, actualSize);

        entityManager.close();
    }

    @Test
    public void testRemoveByIdNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        final int expectedSize = userRepository.getSize().intValue();
        @NotNull final String otherId = UUID.randomUUID().toString();
        entityManager.getTransaction().begin();
        userRepository.removeOneById(otherId);
        entityManager.getTransaction().commit();
        final int actualSize = userRepository.getSize().intValue();
        assertEquals(expectedSize, actualSize);

        entityManager.close();
    }

}
